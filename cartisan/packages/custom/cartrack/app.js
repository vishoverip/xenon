'use strict';

/*
 * Defining the Package
 */
var Module = require('meanio').Module;

var Cartrack = new Module('cartrack');


/*
 * All MEAN packages require registration
 * Dependency injection is used to define required modules
 */
Cartrack.register(function(app, auth, database, http) {

  //We enable routing. By default the Package Object is passed to the routes

    var io = require('./server/config/socketio')(http);

    Cartrack.io=io;

    Cartrack.routes(app, auth, database, io);


    //We are adding a link to the main menu for all authenticated users
  Cartrack.menus.add({
    title: 'cartrack example page',
    link: 'cartrack example page',
    roles: ['authenticated'],
    menu: 'main'
  });

    Cartrack.aggregateAsset('css', 'cartrack.css');

    Cartrack.aggregateAsset('js', '../lib/socket.io-client/socket.io.js', { weight: 1 });

    Cartrack.aggregateAsset('js', '../lib/angular-socket-io/socket.js', { weight: 2 });



  /**
    //Uncomment to use. Requires meanio@0.3.7 or above
    // Save settings with callback
    // Use this for saving data from administration pages
    Cartrack.settings({
        'someSetting': 'some value'
    }, function(err, settings) {
        //you now have the settings object
    });

    // Another save settings example this time with no callback
    // This writes over the last settings.
    Cartrack.settings({
        'anotherSettings': 'some value'
    });

    // Get settings. Retrieves latest saved settigns
    Cartrack.settings(function(err, settings) {
        //you now have the settings object
    });
    */

  return Cartrack;
});


