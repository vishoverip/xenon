'use strict';

/* jshint -W098 */
// The Package is past automatically as first parameter
module.exports = function(Cartrack, app, auth, database, io) {

  app.get('/api/cartrack/example/anyone', function(req, res, next) {
    res.send('Anyone can access this');
  });

  app.get('/api/cartrack/example/auth', auth.requiresLogin, function(req, res, next) {
    res.send('Only authenticated users can access this');
  });

  app.get('/api/cartrack/example/admin', auth.requiresAdmin, function(req, res, next) {
    res.send('Only users with Admin role can access this');
  });

  app.get('/api/cartrack/example/render', function(req, res, next) {
    Cartrack.render('index', {
      package: 'cartrack'
    }, function(err, html) {
      //Rendering a view from the Package server/views
      res.send(html);
    });
  });


    io.on('connection', function(socket) {

        console.log('Chat - user connected');

        socket.on('tractLatLong', function(msg){
            console.log('message: ' + msg);
            /*socket.broadcast.emit('tractLatLong', {
             //   username: socket.username,
                message: msg
            });*/
            io.emit('tractLatLong',msg);

        });


        /***
         * disconnect
         **/
        socket.on('disconnect', function() {
            console.log('Chat - user disconnected');
        });
    });
};

