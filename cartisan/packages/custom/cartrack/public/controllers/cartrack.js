'use strict';

/* jshint -W098 */
angular.module('mean.cartrack', ['btford.socket-io']).controller('CartrackController', ['$scope', 'Global', 'Cartrack', 'mySocket','$timeout',
  function($scope, Global, Cartrack, mySocket, Gmap, $timeout) {
    $scope.global = Global;
    $scope.package = {
      name: 'cartrack'
    };

      // map invocation
      $scope.mymap = {
          center: { // Ness Zionna
              /*        	latitude: 31.9333984 ,
               longitude: 34.8086625*/
              latitude: 12.91233 ,
              longitude: 77.5882
          },
          dragging: false,
          refresh:true,
          zoom: 10
      };

      $scope.mapinst = {
          events: {
              tilesloaded: function (map) {
                  $scope.$apply(function () {
                      $log.info('this is the map instance', map);
                  });
              }
          }
      }
      // Couldn't figure out how panTo shuould work with angular-google-maps so mimicked
      // - an ugly replacement with $timeout.
      $scope.uglyPan = function (){
          console.log("uglyPan!");
          console.log($scope.newcenter);
          $scope.mymap.center = $scope.newcenter;
          $scope.mymap.zoom =10;
      }

      $scope.addMarker= function(location){
          var markerCoords = {
              latitude: location.k,
              longitude: location.B
          };

          $scope.markers.push({coords: markerCoords});
          //  $scope.markets.setIcon('http://maps.google.com/mapfiles/ms/icons/green-dot.png');
          $scope.mymap.zoom = 2;
          $scope.newcenter = markerCoords;
          $timeout($scope.uglyPan,500);
      }

// Multiple marker support

      $scope.options = {scrollwheel: false};
      $scope.markers = [{
          id:0,
          //    icon:{path: 'http://maps.google.com/mapfiles/ms/icons/green-dot.png'},
          coords: { // Jerusalem
              /*                latitude: 31.8615237,
               longitude: 35.1761319*/
              latitude: 12.91233,
              longitude: 77.5882
          }
      },
          {
              id:1,
              //    icon:'http://maps.google.com/mapfiles/ms/icons/blue-dot.png',
              coords: { // Bney Brak
                  /*                latitude: 32.0926177,
                   longitude: 34.8392055*/
                  latitude: 12.92433,
                  longitude: 77.65076
              }

          }
      ];

      $scope.DrawLine={
          pathCords:[],
          stroke:{
              color: '#FF0000',
              weight: 2,
              opacity: 1.0
          },
          clickable: true,
          draggable: true,
          editable: true,
          geodesic: true,
          visible: true,
          fit: true,
          static: true
      };


      mySocket.forward('tractLatLong', $scope);
      $scope.$on('socket:tractLatLong', function (ev, data) {
          var LatLongs = data.split(',');

      //    var $scope.DrawLine.flightPlanCoordinates = new Array();
          /*var point = new google.maps.LatLng(LatLongs[0], LatLongs[1]);
          console.log(point);*/

          var coodes = function(pointers){
              var markerCoords = {latitude: pointers[0], longitude : pointers[1]};
              return markerCoords;
          }
          $scope.DrawLine.pathCords.push(coodes(LatLongs));
          console.log($scope.DrawLine.pathCords);

          //    console.log($scope.DrawLine.stroke.color);

          /*$scope.DrawLine={
              path:flightPlanCoordinates,
              stroke:{
                  color: '#FF0000',
                  weight: 2,
                  opacity: 1.0
              },
              clickable: true,
              refresh: true,
              draggable: true,
              editable: true,
              geodesic: true,
              visible: true,
              fit: true,
              static: true
          };*/
          /*$scope.DrawLine={
              flightPlanCoordinates : [
                  {latitude: 12.91199, longitude: 77.5882},
                  {latitude: 12.91032, longitude: 77.59578},
                  {latitude: 12.9167, longitude: 77.60123},
                  {latitude: 12.9177, longitude: 77.62503},
                  {latitude: 12.92079, longitude: 77.66559},
                  {latitude: 12.92433, longitude: 77.65076}
              ],
              stroke:{
                  color: '#FF0000',
                  weight: 2,
                  opacity: 1.0
              },
              clickable: true,
              draggable: true,
              editable: true,
              geodesic: true,
              visible: true,
              fit: true,
              static: true
          };
          console.log(flightPlanCoordinates);*/
          //    $scope.theData = data;
      });
   //   $scope.DrawLine.setMap($scope.mymap);
   /*   $scope.DrawLine={
          flightPlanCoordinates : [
              {latitude: 12.91199, longitude: 77.5882},
              {latitude: 12.91032, longitude: 77.59578},
              {latitude: 12.9167, longitude: 77.60123},
              {latitude: 12.9177, longitude: 77.62503},
              {latitude: 12.92079, longitude: 77.66559},
              {latitude: 12.92433, longitude: 77.65076}
          ],
          stroke:{
              color: '#FF0000',
              weight: 2,
              opacity: 1.0
          },
          clickable: true,
          draggable: true,
          editable: true,
          geodesic: true,
          visible: true,
          fit: true,
          static: true
      };*/

         /*$scope.DrawLine={
             pathCords : [
       {latitude: 12.91233
           longitude: 77.58820000000003},
       {latitude: 12.91032, longitude: 77.59578},
       {latitude: 12.9167, longitude: 77.60123},
       {latitude: 12.9177, longitude: 77.62503},
       {latitude: 12.92079, longitude: 77.66559},
       {latitude: 12.92433, longitude: 77.65076}
       ],
       stroke:{
       color: '#FF0000',
       weight: 2,
       opacity: 1.0
       },
       clickable: true,
       draggable: true,
       editable: true,
       geodesic: true,
       visible: true,
       fit: true,
       static: true
       };*/
   //   console.log(flightPlanCoordinates);

  }
]);
