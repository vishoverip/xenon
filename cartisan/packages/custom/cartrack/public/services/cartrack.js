'use strict';

angular.module('mean.cartrack').factory('Cartrack', [
  function() {
    return {
      name: 'cartrack'
    };
  }
]);


angular.module('mean.cartrack').factory('mySocket', function (socketFactory) {
    return socketFactory();
})

angular.module('mean.cartrack').factory('Gmap', [
    function() {
        return {
            name: 'gmap'
        };
    }
]);
