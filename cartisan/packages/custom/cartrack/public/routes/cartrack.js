'use strict';

angular.module('mean.cartrack').config(['$stateProvider',
  function($stateProvider) {
    $stateProvider.state('cartrack example page', {
      url: '/cartrack/example',
      templateUrl: 'cartrack/views/index.html'
    });
  }
]);
